//
//  ViewController.h
//  20160426_iOSDay6_ImageView
//
//  Created by ChenSean on 4/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *myImg;
@property (weak, nonatomic) IBOutlet UIImageView *smallImge;

@end

