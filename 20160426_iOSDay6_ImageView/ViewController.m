//
//  ViewController.m
//  20160426_iOSDay6_ImageView
//
//  Created by ChenSean on 4/26/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.myImg.contentMode = UIViewContentModeScaleAspectFill;
    self.myImg.image = [UIImage imageNamed:@"S__8011781.jpg"];
    
    UIImage *image = [UIImage imageNamed:@"S__14475283.jpg"];
    CGSize oldSize = image.size;
    // 將原本圖片大小縮小到 1/10。
    CGSize newSize = CGSizeMake(oldSize.width / 10.0, oldSize.height / 10.0);
    
    UIGraphicsBeginImageContext(newSize);
    // 將畫布重劃
    [image drawInRect:CGRectMake(0, 0, newSize.width - 1, newSize.height - 1)];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    // 將圖收起來
    UIGraphicsEndImageContext();
    
    self.smallImge.image = tmp;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
